function read_data_cell

% Input directory
dr = 'c:\Dropbox\_projects\_Cholinergic_Bloodflow\LSCI1\';
files = dir(dr);
files = files(3:end);

NumFiles = length(files);
for iF = 1:NumFiles
    fnm = files(iF).name;
    [~, ~, ext] = fileparts(fnm);
    if isequal(ext,'.xml')
%         fnm = 'c:\Dropbox\_projects\_Cholinergic_Bloodflow\LSCI1\Serkento DA10 2018-03-08 1058.xml';
        [data, header, raw] = xlsread(fullfile(dr,fnm),'Graph data');
        disp(fnm)
        main(data,header,raw)
    end
end

% -------------------------------------------------------------------------
function main(data,header,raw)

%
markerindex = strcmp(header(1,:),'Markers');
markers = cellfun(@(s)ischar(s),raw(2:end,markerindex));

tinx = strcmp(header(1,:),'Time ms');
timestamps = data(:,tinx);   % time stamps in ms
eiinx = strcmp(header(1,:),'Entire image');
entire_image = data(:,eiinx);   % blood flow
prinx = strcmpi(header(1,:),'1. right PFC');
right_pfc = data(:,prinx);   % right PFC blood flow
plinx = strcmpi(header(1,:),'2. left PFC');
left_pfc = data(:,plinx);   % left PFC blood flow
brinx = contains(header(1,:),'right Barrel');
right_barrel = data(:,brinx);   % right PFC blood flow
blinx = contains(header(1,:),'left Barrel');
left_barrel = data(:,blinx);   % left PFC blood flow

CNO_time = timestamps(markers)';   % CNO application time


%

figure
plot(timestamps,right_barrel,'k')
% ylim([150 210])
hold on
line([CNO_time; CNO_time],ylim,'Color','red')

keyboard

figure
plot(timestamps,smooth(right_barrel,'linear',11))

plf = polyfit(timestamps,right_barrel,3);
figure
plot(timestamps,right_barrel-plf(1)*right_barrel.^3-plf(2)*right_barrel.^2-plf(3)*right_barrel-plf(4))

pord = 17;
plfc = polyfit(timestamps,zscore(right_barrel),pord);
plf = 0;
for k = 1:pord+1
    plf = plf + plfc(k) .* timestamps.^(pord-k+1);
end
figure
plot(timestamps,zscore(right_barrel))
hold on
plot(timestamps,plf)

figure
plot(timestamps,smooth(zscore(right_barrel)-plf,'linear',101))

keyboard

%
srbarrel = smooth(zscore(right_barrel)-plf,'linear',101);
pk = disc(srbarrel,0.5);

ma = movmean(right_barrel,2001);
msd = movstd(right_barrel,2001);
figure;plot(timestamps,ma)
figure;plot(timestamps,ma+msd)

%
% sr = 1000 / mean(diff(timestamps));  % sampling rate
% nqf = sr / 2;   % Nyquist-frequency
% flt = fir1(512,0.005/nqf,'high');
% flbarrel = filtfilt(flt,1,left_barrel);
% figure;plot(timestamps,flbarrel)